#!/bin/sh
# -*- Scheme -*-
exec guile "$0" "$@"
!#
;;; Copyright (C) 2020 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Guile-GLFW.
;;;
;;; Guile-GLFW is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-GLFW is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-GLFW.  If not, see
;;; <https://www.gnu.org/licenses/>.

;;; Commentary:

;;; Generate Scheme procedures definitions from GLFW functions
;;; definitions.

;;; Code:

(use-modules (ice-9 regex)
             (ice-9 rdelim))

(define (glfw->scm definition)
  (let ((m (string-match "^GLFWAPI (.+) (glfw[^(]*)\\(([^)]*)\\);"
                         definition)))

    (define (convert-type type)
      (if (or (string-rindex type #\*)
              (string-match "GLFW[a-z]+fun" type)
              (string= "GLFWglproc" type))
          "'*"
          (let ((m (string-match "(u?int[1-468]{1,2})_t" type)))
            (if m
                (match:substring m 1)
                type))))

    (define (convert-args args)
      (if (string= args "void")
          "'()"
          (string-append
           "(list "
           (string-join (map (lambda (arg)
                               (convert-type
                                (match:substring
                                 (string-match " *([^ ]* ?[^ ]+) [a-zA-Z0-9]+"
                                               arg)
                                 1)))
                             (string-split args #\,))
                        " ")
           ")")))

    (when m
      (let ((return-type (match:substring m 1))
            (name (match:substring m 2))
            (args (match:substring m 3)))
        (when (and return-type name args)
          (string-append "(define-foreign "
                         (string-downcase
                          (regexp-substitute/global #f
                                                    "([a-z]+)([A-Z])"
                                                    name
                                                    'pre
                                                    1 "-" 2
                                                    'post))
                         " "
                         (convert-type return-type)
                         " \"" name "\" "
                         (convert-args args)
                         ")"))))))

(let read-input ((line (read-line)))
  (unless (eof-object? line)
    (display (glfw->scm line))
    (newline)
    (read-input (read-line))))
