;;; Copyright (C) 2020 Andrey Fainer <fandrey@gmx.com>
;;;
;;; This file is part of Guile-GLFW.
;;;
;;; Guile-GLFW is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU Lesser General Public License as
;;; published by the Free Software Foundation, either version 3 of the
;;; License, or (at your option) any later version.
;;;
;;; Guile-GLFW is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;; Lesser General Public License for more details.
;;;
;;; You should have received a copy of the GNU Lesser General Public
;;; License along with Guile-GLFW.  If not, see
;;; <https://www.gnu.org/licenses/>.

(define-module (glfw)
  #:use-module (system foreign)
  #:use-module (rnrs bytevectors)
  #:use-module (glfw bindings)
  #:export (init
            terminate
            call-with-init
            set-error-callback!
            window-hint
            create-window
            destroy-window!
            call-with-window
            window-should-close?
            set-window-should-close!
            get-window-pos
            get-window-size
            set-window-pos-callback!
            set-window-size-callback!
            set-window-maximize-callback!
            set-mouse-button-callback!
            set-cursor-pos-callback!
            mouse-button-left
            mouse-button-right
            mouse-button-middle
            set-key-callback!
            poll-events
            wait-events
            wait-events-timeout
            make-context-current!
            swap-buffers
            get-proc-address))

(define (glfw-error message . args)
  (throw 'glfw-error (apply format #f message args)))

(define (boolean->glfw b)
  (if b GLFW_TRUE GLFW_FALSE))

(define (glfw->boolean b)
  (not (= b GLFW_FALSE)))

(define (glfw-error-code->symbol code)
  (cond
   ((= code GLFW_NO_ERROR)            'glfw-no-error)
   ((= code GLFW_NOT_INITIALIZED)     'glfw-not-initialized)
   ((= code GLFW_NO_CURRENT_CONTEXT)  'glfw-no-current-context)
   ((= code GLFW_INVALID_ENUM)        'glfw-invalid-enum)
   ((= code GLFW_INVALID_VALUE)       'glfw-invalid-value)
   ((= code GLFW_OUT_OF_MEMORY)       'glfw-out-of-memory)
   ((= code GLFW_API_UNAVAILABLE)     'glfw-api-unavailable)
   ((= code GLFW_VERSION_UNAVAILABLE) 'glfw-version-unavailable)
   ((= code GLFW_PLATFORM_ERROR)      'glfw-platform-error)
   ((= code GLFW_FORMAT_UNAVAILABLE)  'glfw-format-unavailable)
   ((= code GLFW_NO_WINDOW_CONTEXT)   'glfw-no-window-context)
   (else 'glfw-unknown-error)))

(define (hint->glfw hint)
  (case hint
    ;; Hint ID
    ((focused)                  GLFW_FOCUSED)
    ((iconified)                GLFW_ICONIFIED)
    ((resizable)                GLFW_RESIZABLE)
    ((visible)                  GLFW_VISIBLE)
    ((decorated)                GLFW_DECORATED)
    ((auto-iconify)             GLFW_AUTO_ICONIFY)
    ((floating)                 GLFW_FLOATING)
    ((maximized)                GLFW_MAXIMIZED)
    ((center-cursor)            GLFW_CENTER_CURSOR)
    ((transparent-framebuffer)  GLFW_TRANSPARENT_FRAMEBUFFER)
    ((hovered)                  GLFW_HOVERED)
    ((focus-on-show)            GLFW_FOCUS_ON_SHOW)
    ((red-bits)                 GLFW_RED_BITS)
    ((green-bits)               GLFW_GREEN_BITS)
    ((blue-bits)                GLFW_BLUE_BITS)
    ((alpha-bits)               GLFW_ALPHA_BITS)
    ((depth-bits)               GLFW_DEPTH_BITS)
    ((stencil-bits)             GLFW_STENCIL_BITS)
    ((accum-red-bits)           GLFW_ACCUM_RED_BITS)
    ((accum-green-bits)         GLFW_ACCUM_GREEN_BITS)
    ((accum-blue-bits)          GLFW_ACCUM_BLUE_BITS)
    ((accum-alpha-bits)         GLFW_ACCUM_ALPHA_BITS)
    ((aux-buffers)              GLFW_AUX_BUFFERS)
    ((stereo)                   GLFW_STEREO)
    ((samples)                  GLFW_SAMPLES)
    ((srgb-capable)             GLFW_SRGB_CAPABLE)
    ((refresh-rate)             GLFW_REFRESH_RATE)
    ((doublebuffer)             GLFW_DOUBLEBUFFER)
    ((client-api)               GLFW_CLIENT_API)
    ((context-version-major)    GLFW_CONTEXT_VERSION_MAJOR)
    ((context-version-minor)    GLFW_CONTEXT_VERSION_MINOR)
    ((context-revision)         GLFW_CONTEXT_REVISION)
    ((context-robustness)       GLFW_CONTEXT_ROBUSTNESS)
    ((opengl-forward-compat)    GLFW_OPENGL_FORWARD_COMPAT)
    ((opengl-debug-context)     GLFW_OPENGL_DEBUG_CONTEXT)
    ((opengl-profile)           GLFW_OPENGL_PROFILE)
    ((context-release-behavior) GLFW_CONTEXT_RELEASE_BEHAVIOR)
    ((context-no-error)         GLFW_CONTEXT_NO_ERROR)
    ((context-creation-api)     GLFW_CONTEXT_CREATION_API)
    ((scale-to-monitor)         GLFW_SCALE_TO_MONITOR)
    ((cocoa-retina-framebuffer) GLFW_COCOA_RETINA_FRAMEBUFFER)
    ((cocoa-frame-name)         GLFW_COCOA_FRAME_NAME)
    ((cocoa-graphics-switching) GLFW_COCOA_GRAPHICS_SWITCHING)
    ((x11-class-name)           GLFW_X11_CLASS_NAME)
    ((x11-instance-name)        GLFW_X11_INSTANCE_NAME)
    ;; Hint value
    ((no-api)                   GLFW_NO_API)
    ((opengl-api)               GLFW_OPENGL_API)
    ((opengl-es-api)            GLFW_OPENGL_ES_API)
    ((no-robustness)            GLFW_NO_ROBUSTNESS)
    ((no-reset-notification)    GLFW_NO_RESET_NOTIFICATION)
    ((lose-context-on-reset)    GLFW_LOSE_CONTEXT_ON_RESET)
    ((opengl-any-profile)       GLFW_OPENGL_ANY_PROFILE)
    ((opengl-core-profile)      GLFW_OPENGL_CORE_PROFILE)
    ((opengl-compat-profile)    GLFW_OPENGL_COMPAT_PROFILE)
    ((cursor)                   GLFW_CURSOR)
    ((sticky-keys)              GLFW_STICKY_KEYS)
    ((sticky-mouse-buttons)     GLFW_STICKY_MOUSE_BUTTONS)
    ((lock-key-mods)            GLFW_LOCK_KEY_MODS)
    ((raw-mouse-motion)         GLFW_RAW_MOUSE_MOTION)
    ((cursor-normal)            GLFW_CURSOR_NORMAL)
    ((cursor-hidden)            GLFW_CURSOR_HIDDEN)
    ((cursor-disabled)          GLFW_CURSOR_DISABLED)
    ((any-release-behavior)     GLFW_ANY_RELEASE_BEHAVIOR)
    ((release-behavior-flush)   GLFW_RELEASE_BEHAVIOR_FLUSH)
    ((release-behavior-none)    GLFW_RELEASE_BEHAVIOR_NONE)
    ((native-context-api)       GLFW_NATIVE_CONTEXT_API)
    ((egl-context-api)          GLFW_EGL_CONTEXT_API)
    ((osmesa-context-api)       GLFW_OSMESA_CONTEXT_API)
    (else (glfw-error "Unknown hint: ~A" hint))))

(define (init)
  (glfw->boolean (glfw-init)))

(define terminate glfw-terminate)

(define (call-with-init proc)
  (dynamic-wind
    init
    proc
    terminate))

(define* (make-set-callback glfw-set-cb-proc args
                            #:optional (callback-around identity) (window? #t))
  ;; Store the callback pointer in a closure.  Otherwise it will be
  ;; garbage collected.
  (let ((ptr #f))
    (if window?
        (lambda (window callback)
          (set! ptr (procedure->pointer void
                                        (callback-around callback)
                                        args))
          (glfw-set-cb-proc window ptr))
        (lambda (callback)
          (set! ptr (procedure->pointer void
                                        (callback-around callback)
                                        args))
          (glfw-set-cb-proc ptr)))))

(define set-error-callback!
  (make-set-callback glfw-set-error-callback
                     (list int '*)
                     (lambda (callback)
                       (lambda (code string)
                         (callback (glfw-error-code->symbol code)
                                   (pointer->string string))))
                     #f))

(define (window-hint hint value)
  (glfw-window-hint (hint->glfw hint)
                    (if (symbol? value)
                        (hint->glfw value)
                        value)))

(define* (create-window width height title #:optional monitor share)
  (glfw-create-window width height
                      (string->pointer title)
                      (if (pointer? monitor) monitor %null-pointer)
                      (if (pointer? share) share %null-pointer)))

(define destroy-window! glfw-destroy-window)

(define (call-with-window window proc)
  (dynamic-wind
    (const #t)
    (lambda () (proc window))
    (lambda ()
      (destroy-window! window))))

(define (window-should-close? window)
  (glfw->boolean (glfw-window-should-close window)))

(define set-window-should-close! glfw-set-window-should-close)

(define (make-get-window proc)
  (lambda* (window #:optional return-list)
    (let ((x (make-bytevector (sizeof int)))
          (y (make-bytevector (sizeof int)))
          (r (if return-list list values)))
      (proc window
            (bytevector->pointer x)
            (bytevector->pointer y))
      (r (s32vector-ref x 0) (s32vector-ref y 0)))))

(define get-window-pos (make-get-window glfw-get-window-pos))
(define get-window-size (make-get-window glfw-get-window-size))

(define set-window-pos-callback!
  (make-set-callback glfw-set-window-pos-callback (list '* int int)))

(define set-window-size-callback!
  (make-set-callback glfw-set-window-size-callback (list '* int int)))

(define set-window-maximize-callback!
  (make-set-callback glfw-set-window-maximize-callback (list '* int)))

(define mouse-button-left GLFW_MOUSE_BUTTON_LEFT)
(define mouse-button-right GLFW_MOUSE_BUTTON_RIGHT)
(define mouse-button-middle GLFW_MOUSE_BUTTON_MIDDLE)

(define (modifier-list mods)
  (let loop ((m `((,GLFW_MOD_SHIFT . shift)
                  (,GLFW_MOD_CONTROL . control)
                  (,GLFW_MOD_ALT . alt)
                  (,GLFW_MOD_SUPER . super)
                  (,GLFW_MOD_CAPS_LOCK . caps-lock)
                  (,GLFW_MOD_NUM_LOCK . num-lock)))
             (l '()))
    (if (null? m)
        (reverse l)
        (loop (cdr m)
              (if (zero? (logand (caar m) mods))
                  l
                  (cons (cdar m) l))))))

(define (glfw-action->symbol action)
  (cond
    ((= action GLFW_RELEASE) 'release)
    ((= action GLFW_PRESS) 'press)
    ((= action GLFW_REPEAT) 'repeat)))

(define set-mouse-button-callback!
  (make-set-callback glfw-set-mouse-button-callback
                     (list '* int int int)
                     (lambda (callback)
                       (lambda (window button action mods)
                         (callback window
                                   button
                                   (glfw-action->symbol action)
                                   (modifier-list mods))))))

(define set-cursor-pos-callback!
  (make-set-callback glfw-set-cursor-pos-callback
                     (list '* double double)))

(let-syntax
    ((reexp (lambda (x)
              (syntax-case x ()
                ((_)
                 (let ((pairs '())
                       (keysym (lambda (str)
                                 (string->symbol
                                  (string-map
                                   (lambda (c)
                                     (if (char=? c #\_) #\- c))
                                   (string-downcase (substring str 5)))))))
                   (module-for-each
                    (lambda (s v)
                      (let ((str (symbol->string s)))
                        (when (string-prefix? "GLFW_KEY_" str)
                          (set! pairs
                                (cons (cons (datum->syntax x s)
                                            (datum->syntax x (keysym str)))
                                      pairs)))))
                    (resolve-module '(glfw bindings)))
                   #`(re-export #,@pairs)))))))
  (reexp))

(define set-key-callback!
  (make-set-callback glfw-set-key-callback
                     (list '* int int int int)
                     (lambda (callback)
                       (lambda (window key scancode action mods)
                         (callback window
                                   key
                                   scancode
                                   (glfw-action->symbol action)
                                   (modifier-list mods))))))

(define poll-events glfw-poll-events)
(define wait-events glfw-wait-events)
(define wait-events-timeout glfw-wait-events-timeout)

(define (make-context-current! window)
  (glfw-make-context-current (if (pointer? window) window %null-pointer)))

(define swap-buffers glfw-swap-buffers)

(define (get-proc-address name)
  (glfw-get-proc-address (string->pointer name)))
