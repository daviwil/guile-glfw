# -*- Autoconf -*-
#
# guile-glfw --- FFI bindings for GLFW
# Copyright © 2022 David Wilson <david@daviwil.com>
#
# This file is part of guile-glfw.
#
# Guile-glfw is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 3 of the
# License, or (at your option) any later version.
#
# Guile-glfw is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with guile-glfw.  If not, see
# <http://www.gnu.org/licenses/>.

AC_INIT(guile-glfw, 0.1.0)
AC_CONFIG_SRCDIR(glfw)
AC_CONFIG_AUX_DIR([build-aux])
AM_INIT_AUTOMAKE([color-tests -Wall -Wno-portability foreign])
AM_SILENT_RULES([yes])

AC_PATH_PROG([GUILE], [guile])
AC_CONFIG_FILES([Makefile glfw/config.scm])
AC_CONFIG_FILES([pre-inst-env], [chmod +x pre-inst-env])

GUILE_PKG([3.0])
GUILE_PROGS

# Core SDL2
PKG_CHECK_MODULES([GLFW3], [glfw3])
PKG_CHECK_VAR([GLFW3_LIBDIR], [glfw3], [libdir])
AC_MSG_CHECKING([GLFW3 library path])
AS_IF([test "x$GLFW3_LIBDIR" = "x"], [
  AC_MSG_FAILURE([unable to find GLFW library directory])
], [
  AC_MSG_RESULT([$GLFW3_LIBDIR])
])
AC_SUBST([GLFW3_LIBDIR])

AC_OUTPUT
